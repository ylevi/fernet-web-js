# Fernet JS

## IMPORTANT
Do note that if you're sending the request manually, you need to send a GET request with the required information as part of the GET headers.

## Getting started

Run the following to start the web server:
``` 
node encryptinator.js
```

Run the following to begin the program and follow the instructions:
``` 
node cli.js
```

