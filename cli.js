const fetch = require('node-fetch')
const inquirer = require('inquirer')

const genUrl = "http://localhost:8000/generate"
const encUrl = "http://localhost:8000/encrypt"
const decUrl = "http://localhost:8000/decrypt"
const gen = "generate"
const enc = "encrypt"
const dec = "decrypt"

async function genKeys() {
    const response = await fetch(genUrl, {
        method: 'GET'
    })

    return response.text()
}

async function encryptMessage(key, encMsg) {
    const response = await fetch(encUrl, {
        method: 'GET',
        headers: {
            uuid: key,
            msg: encMsg
        }
    })

    return response.text()
}

async function decryptMessage(key, decMsg) {
    const response = await fetch(decUrl, {
        method: 'GET',
        headers: {
            uuid: key,
            msg: decMsg
        }
    })

    return response.text()
}


inquirer
    .prompt([
        {
            type: 'rawlist',
            name: 'option',
            message: 'Which actions would you like to perform?',
            choices: ['generate', 'encrypt', 'decrypt'],
        },
    ])
    .then(answers => {
            let choice = answers.option
            if (choice === gen) {
                genKeys().then(result => {
                    console.log("Generated key index is: ", result)
                })
            } else if (choice === enc || choice === dec) {
                inquirer
                    .prompt([
                        {
                            name: 'key',
                            message: 'What is your key index? '
                        },
                        {
                            name: 'msg',
                            message: 'What is your message? '
                        }
                    ])
                    .then(answers => {
                        if (choice === enc) {
                            encryptMessage(answers.key, answers.msg)
                                .then(result => {
                                    console.log("Your encrypted message is: \n", result)
                                }).catch()
                        } else {
                            decryptMessage(answers.key, answers.msg)
                                .then(result => {
                                    console.log("Your decrypted message is: \n", result)
                                })
                        }
                    })
            } else {
                console.log("Invalid output. The program will now exit.")
            }
        }
    )
;