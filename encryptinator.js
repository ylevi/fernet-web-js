const {v4: uuidv4} = require(('uuid'))
const fernet = require('./node_modules/fernet')
const sqlite3 = require('sqlite3').verbose()
const db = new sqlite3.Database('./keys.db')
const express = require('express')
const app = express()
const port = 8000

const KEYGEN = "dd if=/dev/urandom bs=32 count=1 2>/dev/null | openssl base64"

function dbCreator() {
    // TODO - create DB(keys.db), and keys table with columns <uuid | e_key>.
    return ""
}

function keyGen(cmd) {
    const child_process = require('child_process');
    return child_process.execSync(cmd).toString().trim();
}

function getSecret(secret) {
    return new fernet.Secret(secret)
}

function encryptMsg(secret, msg) {
    // TODO - handle encryption errors.
    let encryptedMsg = new fernet.Token({
        secret: secret,
        time: Date.now(),
        iv: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    })

    return encryptedMsg.encode(msg)
}

function decryptMsg(secret, encryptedMsg) {
    // TODO - handle decryption errors.
    let decryptedMsg = new fernet.Token({
        secret: secret,
        token: encryptedMsg,
        ttl: 0
    })

    return decryptedMsg.decode()
}

function insertKey(stringKey) {
    let keyUuid = uuidv4()
    let insert = db.prepare("INSERT INTO keys VALUES (?, ?)")
    insert.run(keyUuid, stringKey)
    insert.finalize()

    return keyUuid
}

const getKeyData = (keyUuid) => {
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.get(`SELECT * FROM keys where uuid = '${keyUuid}'`, (err, rows) => {
                if (err) {
                    reject(err)
                }
                const {e_key} = rows;
                resolve(e_key)
            })
        })
    })
}


app.get('/', function (req, res) {
    res.send('Heya!')
})

app.get('/generate', function (req, res) {
    res.send(insertKey(keyGen(KEYGEN)))
})

app.get('/encrypt', function (req, res) {
    getKeyData(req.headers.uuid).then(result => {
        let secret = getSecret(result)
        res.send(encryptMsg(secret, req.headers.msg))
    })
})

app.get('/decrypt', function (req, res) {
    getKeyData(req.headers.uuid).then(result => {
        let secret = getSecret(result)
        res.send(decryptMsg(secret, req.headers.msg))
    })
})

// TODO - create DB with function before app runs.

app.listen(port, function () {
    console.log("Running app ..")
})

